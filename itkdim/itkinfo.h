#ifndef ITKDIM_ITKINFO_H
#define ITKDIM_ITKINFO_H

#include <ddc/dim/dic.hxx>
#include <string>

class itkinfo : public DimInfoHandler{

 public:
  
  itkinfo();
  
  ~itkinfo();
  
  std::string GetInfo();
  
  void infoHandler();
  
 private:

  DimStampedInfo * m_info;
  
};

#endif 
