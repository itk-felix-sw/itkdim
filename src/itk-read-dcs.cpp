#include <ddc/dim/dis.hxx>
#include <ddc/dim/dic.hxx>
#include <cstdlib>
#include <vector>
#include <map>
#include <string>
#include <cmdl/cmdargs.h>

using namespace std;

int main(int argc , char** argv){

  CmdArgBool    verbose ('v', "verbose","turn on 'verbose' mode.");
  CmdArgStr     name ('n', "name","string", "Application name. Default itk-read-dcs");
  CmdArgStr     host ('H', "host", "hostname", "host running WinCC project. Default itkpix-sr1-st-dcs-01");
  CmdArgStr     port ('P', "port", "number", "port for DIM server. Default 2505");
  CmdArgStr     chain ('c',"chain", "string", "serial powering chain. Example: 'SMI/OB_L3_B03_A_SP2/OB_L3_B03_A_SP2'", CmdArg::isREQ);

  CmdLine cmdl(*argv,&verbose,&name,&host,&port,&chain,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string sname=(name.flags() & CmdArg::GIVEN?string(name):string("itk-read-dcs"));
  string shost=(host.flags() & CmdArg::GIVEN?string(host):string("itkpix-sr1-st-dcs-01"));
  string sport=(port.flags() & CmdArg::GIVEN?string(port):string("2505"));
  string schain=string(chain);
  if(verbose){
    cout << "name: " << sname << endl;
    cout << "host: " << shost << endl;
    cout << "port: " << sport << endl;
  }
  setenv("DIM_DNS_NODE",shost.c_str(),1);
  setenv("DIM_DNS_PORT",sport.c_str(),1);


  //DimCurrentInfo * info = new DimCurrentInfo((char*)"DIS_DNS/SERVER_LIST",(char*)"NOT_RUNNING");
  //cout << "Info: " << info->getString() << endl;

  DimCurrentInfo * info = new DimCurrentInfo((char*)schain.c_str(),(char*)"UNKNOWN");
  if(verbose){
    cout << "DimCurrentInfo (string): " << info->getString() << endl;
  }
  string state(info->getString());
  if(state.find("/")!=string::npos){
    state=state.substr(0,state.find("/"));
  }
  cout << state << endl;
  delete info;
  
  /*
  DimServer * server = new DimServer();
  server->start(sappname.c_str());

    
  for(string name: s_list){
    itkinfo * handler = new itkinfo();
    DimStampedInfo * info = new DimStampedInfo((char*)name.c_str(), (int)-1, handler);
    //m_subs.insert(make_pair(name,info));
  }


  server->stop();
  delete info;
  delete server;
  */

  return 0;
}

