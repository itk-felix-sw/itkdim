#include <itkdim/itkinfo.h>
#include <iostream>
using namespace std;

itkinfo::itkinfo(){
  m_info=0;
}

itkinfo::~itkinfo(){}

void itkinfo::updateInfo(){
  m_ddc_info = (DimStampedInfo *)getInfo();
  cout << "Information updated: " << m_ddc_info->getString() << endl;
}

string GetInfo(){
  return m_info->getString();
}

